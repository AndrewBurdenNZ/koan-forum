<?php

class User_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get_user_info($arg) {
		$query = $this->db->where('user_id', $arg);
		$query = $this->db->get('user');
        return $query->row_array();
	}
	
	function get_user_list() {
		$query = $this->db->get('user');
        return $query->result_array();
	}
	
	function save_to_database($username, $email, $password) {
		$this->db->select_max('user_id'); //I don't know if this is bad practice
		$query = $this->db->get('user');
		$row = $query->row_array(); 
		$maxid = $row['user_id'];
		$maxid++;
		$sha1NewPassword=sha1($this->config->item('code').$password);
		$user_data = array(
			'username' => $username,
			'email' => $email,
			'hash' => $sha1NewPassword,
			'user_id' => $maxid
		);
		
		if($this->db->insert('user', $user_data)) {
			return 1;
		}
		return 0;
	}
	
	function get_user_data_from_database($parameters)
	{
		//$parameters is an array so it produce multiple 'where' clauses
		if (!empty($parameters)) $this->db->where($parameters);
		$query=$this->db->get('users');
		
		foreach ($query->result() as $row)
		{
			$userData=array
			(
			'name'=>$row->name,
			'surname'=>$row->surname,
			'country'=>$row->country,
			'email'=>$row->email,
			'activated'=>$row->activated
			);
			$users[]=$userData;
		}
		if (!empty($users))	return $users;
	}
	
	function update_activation_status($parameter)
	{
		$this->db->where('email', $parameter);
		$this->db->set('activated', 1);
		$this->db->update('users');
	}
	
	function check_login_data($username, $password)
	{
		$this->db->where('username', $username);
		$userByName = $this->db->get('user');
		$row = $userByName->row();
		
		if ($row)
		{
			if (sha1($this->config->item('code').$password)===$row->hash)
			{
				$this->set_session($row);
				return "logged_in";
				/*if ($row->acct_activated==1) 
				{
					$this->set_session($row);
					return "logged_in";
				}
				else 
				{
					return "not_activated";
				}*/
			}
			else
			{
				return "wrong_password";
			}
		}
		else
		{
			return "no_user";
		}
	}
	
	function change_password($email, $password)
	{
		$sha1NewPassword=sha1($this->config->item('code').$password);
		
		$this->db->where('email', $email);
		$this->db->set('password', $sha1NewPassword);
		$result=$this->db->update('users');
	}
	
	function verify_email_code($email, $email_code)
	{
		$array=array('email'=>$email);
		$user_data=$this->get_user_data_from_database($array);
		
		if (sha1($this->config->item('code').$user_data[0]['name'])===$email_code) return TRUE; else return FALSE;
	}
	
	function edit_profile($user_email, $update_item)
	{
		$this->db->where('email', $user_email);
		$this->db->set($update_item);
		$result=$this->db->update('users');
	}
	
	function set_session($row)
	{
		$user=array(
		'username'=>$row->username,
		'email'=>$row->email,
		'logged_in' => TRUE,
		'user_id' => $row->user_id,
		'is_admin' => $row->admin
		);
		if (!empty($user)) $this->session->set_userdata($user);
	}
	
/*
	public function send_activating_email($receipent)
	{
		$conf=$this->config->item('emailSettings');
		$this->email->initialize($conf);
		
		$activating_code=md5($this->config->item('code').$receipent);
		
		$message='<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
				"http://www.w3.org/TR/html4/strict.dtd">
				
				<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<title>HTML</title>
						<meta name="author" content="otmek" />
						<!-- Date: 2013-05-06 -->
					</head>
					<body>';
		$message.='Click <a href="'.base_url().'user/activate_account/'.$receipent.'/'.$activating_code.'">here</a> to activate your account.';
		$message.='</body>
				</html>';
		
		$this->email->from($conf['smtp_user'], 'Website Admin');
		$this->email->subject('Activate your account');
		$this->email->to($receipent);
		$this->email->message($message);
		$sent=$this->email->send();
		if ($sent===FALSE) echo $this->email->print_debugger();
	}

	public function send_email_reset_password($requesting_user)
	{
		$conf=$this->config->item('emailSettings');
		$this->email->initialize($conf);
		
		$name=$requesting_user['name'];
		$receipent=$requesting_user['email'];
		$email_code=sha1($this->config->item('code').$name);
		
		$message='<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
				"http://www.w3.org/TR/html4/strict.dtd">
				
				<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<title>HTML</title>
						<meta name="author" content="otmek" />
						<!-- Date: 2013-05-06 -->
					</head>
					<body>';
		$message.='Click <a href="'.base_url().'user/view_update_password/'.$receipent.'/'.$email_code.'">here</a> to change your password.';
		$message.='</body>
				</html>';
		
		$this->email->from($conf['smtp_user'], 'Website Admin');
		$this->email->subject('We got request to reset your password');
		$this->email->to($receipent);
		$this->email->message($message);
		$sent=$this->email->send();
		if ($sent===FALSE) echo $this->email->print_debugger();
	}*/
}
