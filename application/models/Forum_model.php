<?php

class Forum_model extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
		function get_category_list() {
		$query = $this->db->get('category');
        return $query->result_array();
	}
	
	function get_forum_list() {
		$query = $this->db->get('forum');
        return $query->result_array();
	}
	
	function get_forum_list_by_category($category) {
		$this->db->where('category_id', $category);
		$query = $this->db->get('forum');
        return $query->result_array();
	}
	
	function get_topic_list($category) {
		$this->db->where('topic.category_id', $category);
		$this->db->join('post', 'post.topic_id = topic.topic_id');
		$this->db->join('user', 'post.user_id = user.user_id');
		$query = $this->db->get('topic');
        return $query->result_array();
	}
	
	function get_topic_info($topic_id) {
		$this->db->where('topic.topic_id', $topic_id);
		$this->db->join('post', 'post.topic_id = topic.topic_id');
		$query = $this->db->get('topic');
		return $query->row_array();
	}
	
	function get_posts($topic_id, $current_page) {
		$this->db->where('topic_id', $topic_id);
		$this->db->join('user', 'post.user_id = user.user_id');
		$query = $this->db->get('post', 10, 10 * $current_page - 10);
		return $query->result_array();
	}
	
	function create_topic($category_id, $title, $body, $user_id) {
		$this->load->helper('date');
		$this->db->select_max('topic_id'); //I don't know if this is bad practice
		$query = $this->db->get('topic');
		$row = $query->row_array(); 
		$maxid = $row['topic_id'];
		$maxid++;
		$this->create_post($maxid, $title, $body, $user_id);
		$this->db->select_max('post_id'); //I don't know if this is bad practice
		$query = $this->db->get('post');
		$row = $query->row_array(); 
		$maxid = $row['post_id'];
		$topic_data = array(
			'post_id' => $maxid,
			'category_id' => $category_id,
		);
		$this->db->insert('topic', $topic_data);
	}
	
	function create_post($topic_id, $title, $body, $user_id) {
		$this->load->helper('date');
		$datestring = "%Y-%m-%d %h:%i:%a";
		$time = time();
		$post_data = array(
			'post_title' => $title,
			'user_id' => $user_id,
			'topic_id' => $topic_id,
			'post_body' => $body,
			'post_edit_date' => mdate($datestring, $time),
			'hidden' => 0
		);
		$this->db->insert('post', $post_data); 
	}
	
	function delete_topic($topic_id, $user_id) {
		$this->load->helper('date');
		$datestring = "%Y-%m-%d %h:%i:%a";
		$time = time();
		$post_data = array(
			'user_id' => $user_id,
			'topic_id' => $topic_id,
			'body' => $body,
			'date' => mdate($datestring, $time),
			'hidden' => 0
		);
		$this->db->insert('post', $post_data); 
	}
}
