<!DOCTYPE html>
<html>
	<head>
		<title>Koan Forum</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link href="<?php echo base_url('/'); ?>css/bootstrap.min.css?<?php echo date('YmdH'); ?>" rel="stylesheet" media="screen">
		<link href="<?php echo base_url('/'); ?>css/header.css?<?php echo date('YmdH'); ?>" rel="stylesheet" media="screen">
		<link href="<?php echo base_url('/'); ?>css/main.css?<?php echo date('YmdH'); ?>" rel="stylesheet" media="screen">
		
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="<?php echo base_url('/'); ?>js/bootstrap.min.js?<?php echo date('YmdH'); ?>"></script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="masthead">

                    <div class="navbar navbar-default navbar-static-top" role="navigation">
                        <div class="container">
                            <div class="navbar-inner">
                                <h3 class="text-muted">Koan Forum</h3>
                                <a class="navbar-brand" href="/" ><img src="images/banner.png" title="KoanForum" alt="KoanForum"></a>
                            </div>
                            <div class="navbar-collapse collapse">
                                    <?php if($this->session->userdata('logged_in') != TRUE) {
                                        echo '<div style="float:right">
                                <a href="';
                                        echo base_url('/user/register');
                                        echo '" >Register</a><br/>
                                <form class="navbar-form form-inline" role="form" action="';
                                        echo base_url('/user/login/');
                                        echo '" method="POST">
                                    <div class = "form-group">
                                        <label class="sr-only">Username</label>';
                                        $username_attributes = array('name' => 'username', 'placeholder' => 'Username', 'class' => 'form-control');
                                        echo form_input($username_attributes);
                                        echo '</div>
                                    <div class = "form-group">
                                        <label class="sr-only">Password</label>';
                                        $password_attributes = array('name' => 'password', 'placeholder' => 'Password', 'class' => 'form-control');
                                        echo form_password($password_attributes);
                                        echo '</div>';
                                        form_hidden('url', '/' . $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' );
                                        echo '<button type="submit" class="btn btn-default">Log in</button>
                                </form>
                            </div>';
                                    } else {

                                        echo '<div style = "float:right">Welcome, ' . $this->session->userdata('username') . '!';
                                        if($this->session->userdata('is_admin') == true) {
                                            echo '<a class="btn btn-default" href="' . base_url('/admin/') . '">Admin</a>';
                                        }
                                        echo '<form class="navbar-form form-inline" method="post" action="' . base_url('/user/logout/') . '"><button type="submit" name="logout" class="btn btn-default">Logout</button></form></div>';
                                    }?>
                                    <ul class="nav">
                                        <li<?php if($current_menu_item == 'home') { echo ' class="active"'; } ?>>
                                            <a href="<?php echo base_url('/'); ?>">Home</a>
                                        </li>
                                        <li<?php if($current_menu_item == 'forum') { echo ' class="active"'; } ?>>
                                            <a href="<?php echo base_url('/forum'); ?>">Forum</a>
                                        </li>
                                        <li<?php if($current_menu_item == 'download') { echo ' class="active"'; } ?>>
                                            <a href="<?php echo base_url('/download'); ?>">Downloads</a>
                                        </li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- /.menu -->
					<nav class="navbar navbar-default" role="navigation">
						<div class="navbar-inner">

						</div>
				</nav>
			</div>
			<!----div with success/errors messages---->
			<?php
			$all_messages = $this->messages->get();
			?>
			
			
			<br style="clear: both" />
			<div class="row-fluid">