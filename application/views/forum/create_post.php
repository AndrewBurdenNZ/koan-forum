<h2>Reply!</h2>

<form class="form-horizontal" role="form" action="<?php echo base_url('/forum/submit/') . '/' . $topic_id; ?>" method="post">
	<div class="form-group">
		<div class="col-sm-5">
			<input type="hidden" name="type" value="post">
			<input type="hidden" name="topic_id" value="<?php echo $topic_id;?>">
			<input type="text" class="form-control" name="title" id="title" placeholder="Post Title" value="Re: <?php echo $topic_info['post_title']; ?>">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-10">
			<textarea class="form-control" rows="8" name="body" id="body"></textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-0 col-sm-10">
			<button type="submit" class="btn btn-default">Submit</button>
		</div>
	</div>
</form>