<h3><?php echo $topic_info['post_title']; ?></h3>
<table class = 'table'>
	<thead>
		<tr>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php 
			foreach($post as $row) {
				echo '<div class="row"><div class="col-md-4">';
				echo '<tr><td><a href="' . base_url('/user/profile/') . '/' . $row['user_id'] . '">' . $row['username'] . '</a></td></tr>';
				echo '</div><div class="col-md-4"></div><div class="col-md-4"></div></div>';
				echo '<div class="row"><div class="col-md-4"><tr><td></td><td>' . $row['post_body'] . '</td></tr>';
				echo '</div><div class="col-md-4"></div><div class="col-md-4"></div></div>';
			}
		?>
	</tbody>
</table>
<?php if(!$this->session->userdata('username')) {
	echo "Log in or register to reply.";
} else {
	echo '
	<form method="GET" action="' . base_url('/forum/create_post/') . '/' . $topic_id . '">
		<div class="col-sm-10">
			<button type="submit" class="btn btn-default">Reply</button>
		</div>
	</form>';
}