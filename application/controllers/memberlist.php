<?php

class Memberlist extends CI_Controller {
	
	function MemberList()
	{
		parent::__construct();
	}
	
	function index() {
		$data['current_menu_item'] = '';
		$this->load->view('shared/header', $data);
		$this->load->model('User_model');
		$data['users'] = $this->User_model->get_user_list();
		$this->load->view('memberlist/index', $data);
		$this->load->view('shared/footer');
	}
}