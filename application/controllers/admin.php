<?php

class Admin extends CI_Controller {
	
	function Admin() {
		parent::__construct();
	}
	
	function index() {
		if($this->session->userdata('is_admin') == 0) {
			$data['current_menu_item'] = null; // show nothing in the top menu
			$this->load->view('shared/header', $data);
			$this->load->view('shared/forbidden');
			$this->load->view('shared/footer');
		} else {
			$data['current_menu_item'] = null; // show nothing in the top menu
			$this->load->view('shared/header', $data);
			$this->load->view('admin/menu', $data);
			$this->load->view('admin/index', $data);
			$this->load->view('shared/footer');
		}
	}
	
}

?>
