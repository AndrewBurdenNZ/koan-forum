<?php

class Download extends CI_Controller {
	
	function Download()
	{
		parent::__construct();
	}
	
	function index() {
		$data['current_menu_item'] = 'download';
		$this->load->view('shared/header', $data);
		$data = array();
		$this->load->view('download/index', $data);
		$this->load->view('shared/footer');
	}
}