<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}
	
	function index() {
		$data = array();
		$arg = $this->uri->segment(3);
		$this->load->model('User_model');
		$data['current_menu_item'] = '';
		$this->load->view('shared/header', $data);
		$this->load->view('user/index', $data);
		$this->load->view('shared/footer', $data);
	}
	
	function profile() {
		$data = array();
		$this->load->model('User_model');
		$arg = $this->uri->segment(3);
		$data['current_menu_item'] = '';
		$this->load->view('shared/header', $data);
		//General information grabbing for display
		$data['user_info'] = $this->User_model->get_user_info($arg);

		//Load the profile page view
		$this->load->view('user/profile', $data);
		$this->load->view('shared/footer', $data);
	}

	public function register() {
		$data['current_menu_item'] = '';
		if (empty($_POST))
		{
			$this->load->view('shared/header', $data);
			
			$this->load->view('user/register_form');
			
			$this->load->view('shared/footer');
		}
		else
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|max_length[14]|is_unique[user.username]|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[7]|max_length[50]|valid_email|is_unique[user.email]|xss_clean');
			//$this->form_validation->set_rules('conf_email', 'Confirmed Email', 'trim|required|valid_email|min_length[7]|max_length[50]|matches[email]|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('password_confirmation', 'Confirmed password', 'trim|required|min_length[6]|max_length[50]|matches[password]|xss_clean');
			
			if ($this->form_validation->run()==FALSE)
			{
				$this->load->view('shared/header', $data);
				$data['message'] = "Invalid input";
				$this->load->view('user/register_form', $data);
			
				$this->load->view('shared/footer');
			}
			else
			{
				$this->load->model('User_model');
				$saved=$this->User_model->save_to_database(
					$this->input->post('username', TRUE), 
					$this->input->post('email', TRUE),
					$this->input->post('password', TRUE)
				);
				//$this->User_model->send_activating_email($user_data['email']);
				//$this->messages->set_message('Account registered correctly. Please activate it through instructions send to your e-mail address');
				redirect(base_url(), 'refresh');
			}
		}
	}

	public function login() {
		$this->load->model('User_model');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[6]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[50]|xss_clean');
		if ($this->form_validation->run()==FALSE)
		{	
			$this->index();
		}
		else
		{			
			$logged_in = $this->User_model->check_login_data($this->input->post('username', TRUE), $this->input->post('password', TRUE));
			
			switch ($logged_in)
			{
				case "logged_in":
					$this->messages->add('Successful login, ', 'success');
					echo "logged in";
				break;
				case "not_activated": 
					$this->messages->add('Your account is not activated', 'error');
					echo "not activated";
				break;
				
				case "wrong_password": 
					$this->messages->add('Wrong password. Please Retrieve Password','error');
					echo "wrong password";
				break;
				
				case "no_user":
					$this->messages->add('No such an user. Please correct your e-mail address','error');
					echo "no user";
				break;
			}
			
			redirect(base_url($this->input->post('url', FALSE)), 'refresh');
		}
	}
	
	function logout() {
		if(isset($_POST['logout'])) { 
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('activated');
		$this->session->unset_userdata('is_admin');
		$this->session->sess_destroy();
		}
		redirect(base_url('/'), 'refresh');
	}
}