<?php

class Forum extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$data = array();
		$this->load->model('Forum_model');
		$data['current_menu_item'] = 'forum';
		$this->load->view('shared/header', $data);
		
		$data['category_list'] = $this->Forum_model->get_category_list();	
		
		$this->load->view('forum/index', $data);
		$this->load->view('shared/footer');
	}
	
	function category()
	{
		$data = array();
		$this->load->model('Forum_model');
		$data['current_menu_item'] = 'forum';
		$this->load->view('shared/header', $data);
		
		$data['category_id'] = $this->uri->segment(3);
		$data['topic_list'] = $this->Forum_model->get_topic_list($this->uri->segment(3));
		$this->load->view('forum/category', $data);
		$this->load->view('shared/footer');
	}
	
	function topic()
	{
		$data = array();
		$this->load->model('Forum_model');
		$data['current_menu_item'] = 'forum';
		$topic_id = $this->uri->segment(3);
		if($this->uri->segment(4) != NULL) {
			$current_page = $this->uri->segment(4);
		} else {
			$current_page = 1;
		}
		
		$this->load->view('shared/header', $data);
		
		$data['topic_id'] = $topic_id;
		$data['post'] = $this->Forum_model->get_posts($topic_id, $current_page);	
		$data['topic_info'] = $this->Forum_model->get_topic_info($topic_id);
		
		$this->load->view('forum/topic', $data);
		$this->load->view('shared/footer');
	}
	
	function create_topic()
	{
		$this->load->helper('form');
		$data = array();
		$this->load->model('Forum_model');
		$data['current_menu_item'] = 'forum';
		$arg = $this->uri->segment(3);
		$this->load->view('shared/header', $data);
		
		if($arg == NULL) {
			
		} else {
			$data['category_id'] = $arg;
			$data['topic_list'] = $this->Forum_model->get_topic_list($arg);	
			$this->load->view('forum/create_topic', $data);
		}
		
		$this->load->view('shared/footer');
	}
	
	function delete_topic()
	{
		$this->load->helper('form');
		$data = array();
		$this->load->model('Forum_model');
		$data['current_menu_item'] = 'forum';
		$arg = $this->uri->segment(3);
		$this->load->view('shared/header', $data);
		
		if($arg == NULL) {
			
		} else {
			if($this->session->userdata('is_admin') == "true") {
				
			}
		}
		
		$this->load->view('shared/footer');
	}
	
	function create_post()
	{
		$this->load->helper('form');
		$data = array();
		$this->load->model('Forum_model');
		$data['current_menu_item'] = 'forum';
		$topic_id = $this->uri->segment(3);
		$this->load->view('shared/header', $data);
		
		if($topic_id == NULL) {
			
		} else {
			$data['topic_info'] = $this->Forum_model->get_topic_info($topic_id);
			$data['topic_id'] = $topic_id;
			$this->load->view('forum/create_post', $data);
		}
		
		$this->load->view('shared/footer');
	}
	
	function submit()
	{
		$data = array();
		$this->load->model('Forum_model');
		$data['current_menu_item'] = 'forum';
		$this->load->view('shared/header', $data);
		if($this->input->post('type') == 'topic') {
			echo "form data is topic";
			$this->Forum_model->create_topic($this->input->post('category_id'), $this->input->post('title'), $this->input->post('body'), $this->session->userdata('user_id'));
		} else if($this->input->post('type') == 'post') {
			$this->Forum_model->create_post($this->input->post('topic_id'), $this->input->post('title'), $this->input->post('body'), $this->session->userdata('user_id'));
		}
	}
}
