<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function Home()
	{
		parent::__construct();
	}
		
	public function index() {
		$data['current_menu_item'] = 'home';
		$this->load->view('shared/header', $data);
		$data['message'] = 'PHP can go here too!';
		
		$this->load->view('home/index', $data);
		
		$this->load->view('shared/footer');
	}
}